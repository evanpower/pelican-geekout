# geekout #
Geekout is a theme for the [Pelican](https://getpelican.com) static site
generator. It is a slightly modified version of the 
[dev-random2](https://github.com/getpelican-themes/dev-random2) theme by 
Sam Hocevar.

